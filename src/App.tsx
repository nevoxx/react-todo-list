import React, {useState} from "react";
import {TodoList} from "./TodoList";
import {TodoForm} from "./TodoForm";
import {Todo, ToggleComplete, AddTodo} from "./types";

// Todo: get from api and stuff ...
const initialTodos: Array<Todo> = [
    {
        text: "Sterni trinken",
        complete: true
    },
    {
        text: "Mehr Sterni trinken",
        complete: false
    }
];

const App: React.FC = () => {
    const [todos, setTodos] = useState<Array<Todo>>(initialTodos);

    const toggleComplete: ToggleComplete = selectedTodo => {
        const updatedTodos = todos.map(todo => {
            if (todo === selectedTodo) {
                return {...todo, complete: !todo.complete};
            }
            return todo;
        });
        setTodos(updatedTodos);
    };

    const addTodo: AddTodo = newTodo => {
        if (newTodo.trim() !== "") {
            setTodos([...todos, {text: newTodo, complete: false}]);
        }
    };

    return (
        <div>
            <TodoList todos={todos} toggleComplete={toggleComplete}/>
            <TodoForm addTodo={addTodo}/>
        </div>
    );
};

export default App;